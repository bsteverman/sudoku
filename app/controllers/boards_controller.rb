class BoardsController < ApplicationController

  layout  'application'

	def index
    @boards = Board.sorted
  end

  def show
    @board = Board.find params[:id]
  end

  def new
    @board = Board.new name: "Name"
    @board_count = Board.count + 1
  end

  def create
    # Instantiate a new object using form parameters
    @board = Board.new board_params
    # Save the object
    if @board.save
      # If save succeeds, redirect to the index action
      flash[:notice] = "Board created successfully."
      redirect_to action: 'index'
    else
      # If save fails, redisplay the form so user can fix problems
      @board_count = Board.count + 1
      render 'new'
    end
  end

  def edit
    @board = Board.find params[:id]
    @board_count = Board.count
  end

  def update
    # Find an existing object using form parameters
    @board = Board.find params[:id]
    # Update the object
    if Board.update( @board.id, board_params) 
      # If update succeeds, redirect to the index action
      flash[:notice] = "Board updated successfully."
      redirect_to action: 'show', id: @board.id
    else
      # If update fails, redisplay the form so user can fix problems
      @board_count = Board.count
      render 'edit'
    end
  end

  def delete
    @board = Board.find params[:id]
  end

  def destroy
    board = Board.find(params[:id]).destroy
    flash[:notice] = "Board '#{board.name}' destroyed successfully."
    redirect_to action: 'index'
  end


  private
    def board_params
      # same as using "params[:board]", except that it:
      # - raises an error if :board is not present
      # - allows listed attributes to be mass-assigned
      params.require(:board).permit(:name, :positions, :flags, :current_version)
    end
end
