Starter rails application: Sudoku

Single page. Features Hint and Solve functions. Uses grape/rest api. Just beginning to look at algorithms to solve 'like people do'.

Assuming rails is installed, just download, bundle install, copy /config/database.yml.sample to /config/database.yml, rake db:migrate, rake loadDB, and rails s.

Many things could be done with this program. Comments and suggestions are welcome.