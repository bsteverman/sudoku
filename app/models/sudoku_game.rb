
class SudokuGame

	attr_reader :board
	attr_reader :sections

	def initialize boardString = nil
		## board is simple array of 81 numbers. 
		## Initializing to the number 0 through 81 is temporary. 
		## Wanted these values as the thing is constructed as it allows ad hoc debug printouts
		@board = Array (0..81)
		@boardSolution = []

		## sections represent the rows, the columns, and the nine square sections of the game.
		## So there are 27 sections - all need digits 1 through 9 no dupes:
		## => Nine Rows
		## => Nine Columns
		## => Nine squares
		##
		## Sections is an array of arrays. each array entry represents a section. 
		## Section values are indices into the @board[]... 
		## So the first array in sections (which is the first row) contains (0..8), the second (9..17)..
		## The nine 'square' sections are the trickiest
		##
		## Populating these with little loops rather than typing out each section explicilty.
		## some of the populations are a bit tricky but it seemed easier to do it this way. 
		@sections = []
		
		a = []
		
		##
		## Helpful to visualize the board like this:
		##
		##  0, 1, 2, 3, 4, 5, 6, 7, 8,
		##  9,10,11,12,13,14,15,16,17,
		## 18,19,20,21,22,23,24,25,26,
		## 27,28,29,30,31,32,33,34,35,
		## 36,37,38,39,40,41,42,43,44,
		## 45,46,47,48,49,50,51,52,53,
		## 54,55,56,57,58,59,60,61,62,
		## 63,64,65,66,67,68,69,70,71,
		## 72,73,74,75,76,77,78,79,80,
		##
		
		## Initialize row sections.
		## For rows 0 thru 8, populate a[i] with the index into @board that points to the ith element of that row.
		## Not hard if you visualize the board as above.
		##
		(0..8).each do |row| 
			(0..8).each { |i| a[i] = row*9+i } 
			@sections.push(a.dup)		# Need a copy otherwise each array is just a reference to whatever a[] is at the end of the method.
		end

		## Initialize column sections.
		## Very similar to rows but jumping by 9s going down the board.
		## 
		(0..8).each do |column|
			(0..8).each { |i| a[i] = column+(i*9) } 	
			@sections.push(a.dup)
		end

		## Initialize the 9 square sections.
		## 3x3 | 3x3 | 3x3 |
		## -----------------
		## 3x3 | 3x3 | 3x3 |
		## -----------------
		## 3x3 | 3x3 | 3x3 |
		##

		(0..2).each do |cornerY|			# top boxes, middle boxes, bottom boxes
			 (0..2).each do |cornerX| 		# left box, middle box, right boxes
	
			 		cX =cornerX*3
			 		cY =cornerY*3
					a = []
			 		(cY..cY+2).each do |row| 
			 				a.push( row*9+cX)
			 				a.push( row*9+cX+1)
			 				a.push( row*9+cX+2)
			 		end	
			 		@sections.push(a.dup)		
			end
		end

		loadBoard( boardString ) unless boardString.nil?
	end

	def board_to_s
		s = "board:\n"
		s += "  | 1  2  3 | 4  5  6 | 7  8  9 |\n"
		s += "  +-----------------------------+\n"

		(0..8).each do |row|
				s += "#{row+1} |"
			(0..8).each do |col|
				s += @board[row*9+col].to_s.rjust(2) + " "
				if (col+1)%3 == 0 then s += "|" end
			end
			s += "\n"
			if (row+1)%3 == 0 then
				s += "  +-----------------------------+\n"
			end	
		end
		s
	end

	def sections_to_s
		s = "sections:\n"
		@sections.each { |arr| s += arr.to_s + "\n" }
		s
	end

	def to_s
		 #s = @board.inspect
		 self.board_to_s + "\n\n" + self.sections_to_s
	end

	def indexToRowColumn idx
		return (idx/9).to_i , idx%9
	end

	def loadGame fileName
		buffer = File.read(fileName)		# Read whole file
		loadBoard(buffer)
	end	

	def loadBoard boardString
		boardString.gsub!(/\r/,"")				# eliminate EOL
		boardString.gsub!(/\s+/, "")			# Eliminate white space
		boardString.gsub!(",", "")				# Eliminate comma

		@board = boardString.scan(/./).map { |s| s.to_i }				# Split into single char strings

		@board
	end	

	def boardStr
		boardStrg = ""
		@board.each { |i| boardStrg += i.to_s}
		boardStrg

	end	

	def canBe row, col, value
		cell = row*9+col 					# @board[cell] is where cell referenced sits.
		
		return(false) unless 0 <= cell && cell < 81
		return(false) unless @board[cell] == 0

		@sections.each do |sec|
			if sec.include?(cell)
				#puts sec.inspect
				sec.each { |i| if (@board[i] == value) then return(false) end }
			end
		end
		return true
	end

	def mustBe row, col, value
		## find out if 'value' must be in (row,col) by showing that
		## --> The value can go into the cell - ie: It doesn't create the same value in a row, column, or square.
		## --> The value will not fit into any other cell within the current row., or column, or square.

		## Simple case - this value can be in (row,col) cell.
		return(false) unless canBe( row, col, value)

		## If this value cannot be placed anywhere else in one of the sections its in,
		## 		it must belong at the suggested location.
		## 
		## Remember @sections are the individual rows, columns, and square sections of Sudoku. 
		## Represented as an array of sections. Each section is an array of indexes into @board.
		## Not so hard if you can see that, say, the left middle square section would be the 'sec' array:
		##
		## sec = [18, 19, 20,
		## =>     27, 28, 29,
		## =>     36, 37, 38 ]
		##

		cell = row*9+col 					# @board[cell] is where cell referenced sits.
		
		# only process sections that include this cell
		# there will always be three sections for the row, column, and square
		# Always three of these - a row, a column, and a square section.
		subsecs = @sections.select { |sec| sec.include?(cell) }	

		subsecs.each do |sec|

			# Look at every cell in this section except for the one we are trying to place.
			# If our value won't fit in any other square in the section,
			# it must go where the caller says - ie: mustBe(row,col,value) is true
			#
			sec = sec - [cell]			# strip out the target cell

			 if ( allCannotBe(sec,value) == true) then return(true) end

		end	
		return false
	end

	## Utility function for readability.
	## allCannotBe(sec[], value) - checks if ALL cells with a 'square, row, or column' cannot be a particular value.
	## if ALL squares Cannot hold the value, it returns TRUE.
	##
	## term 'square, row, or column' not technically accurate as it check array of offsets into @board.
	## the section passed is not the whole section and can be any set of offsets.
	##
	def allCannotBe sec, value
		sec.each do |i| 
			secRow, secCol = self.indexToRowColumn(i)
			if (canBe(secRow, secCol, value) == true ) then return(false) end 
		end
		return true
	end

	## The idea with this here hint function is to try to solve for the next move the way a human would.
	## Have not developed anything very sophisticated at all (yet).
	## The final method is brute force - which cheats by 'peeking at the solution'
	## 
	## There is a bunch of information on the web about different approaches.
	##
	## Note - putting results to rails console is a bit of a hack - but the idea was the user probably
	## doesn't care as much as I do how the hint was obtained.
	## Future versions may incorporate a UI which would tell the user how the hint was obtained.
	## All pretty rudimentary for now.
	##
	def hint
		r, c, v = findMoveMustBe
		puts 'Must-Be logic: ' + ( v == 0 ? "Failed" : "Succeeded")

		if( v == 0) then 
			r, c, v = findMoveNoOtherValue
			puts 'No-Other-Value logic: ' + ( v == 0 ? "Failed" : "Succeeded")
		end

		if( v == 0) then 
			r, c, v = findMoveBruteForce
			puts 'Brute-Force logic: ' + ( v == 0 ? "Failed" : "Succeeded")
		end

		return r,c,v
	end

	def findMoveMustBe
		(0..80).each do |idx|

			if (@board[idx] == 0) then

				row, col = indexToRowColumn(idx)

				(1..9).each do |val|
					if mustBe(row,col,val) == true then
						return row, col, val
					end
				end
			end
		end
		return 0,0,0
	end

	def findMoveNoOtherValue
		(0..80).each do |idx|

			if (@board[idx] == 0) then

				row, col = indexToRowColumn(idx)

				(1..9).each do |val|
					if mustBeSinceNoOtherNumberFitsInThatSquare(row,col,val) == true then
						return row, col, val
					end
				end
			end
		end
		return 0,0,0
	end

	def findMoveBruteForce
		#
		# brute force - find first unfilled cell, solve game, and return cell value.
		#
		(0..80).each do |idx|

			if (@board[idx] == 0) then

				row, col = indexToRowColumn(idx)

				b = SudokuGame.new( boardStr )
				
				if(b.solve == true) then
					value = b.board[idx]
					return row, col, value
				end
			end
		end
		return 0,0,0
	end		

	def setValue row, col, value
		cell = row*9 + col
		if @board[cell] != 0 then return(false) end
		@board[cell] = value
		return true
	end

	# 
	def mustBeSinceNoOtherNumberFitsInThatSquare row, col, value
		return(false) unless canBe( row, col, value)

		(1..9).each { |i| if ( i != value && canBe(row, col, value) == true) then return(false) end }

		return true
	end

	def solve
		idx = @board.find_index { |v| v==0 }		# Find a cell with a zero in it

		if(idx == nil) then return true end			# no zeros means all done!

		r,c = indexToRowColumn(idx)

		## try a 1, try a 2, try a 3 ...

		(1..9).each do |v|

			if canBe(r,c,v) then
				@board[idx] = v
				if solve() == true then return true end
				@board[idx] = 0								# undo last guess
			end
		end

		# tried 1 thru 9 - nothing worked
		return false
	end

end	
