Rails.application.routes.draw do

mount Player::Play => '/api'

root "boards#index"

#get "demo/index"
match ':controller(/:action(/:id))', :via => [:get, :post]

end
 