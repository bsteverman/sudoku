

// Global Variables
var allBoards = null;
var currentBoard = null;
var current_cell = null; // the currently selected cell


// Bootstrap when page is loaded
$("document").ready(function() {
	initPage();
});

//
// Called on Page load AND to load/reload a game.
//
function initPage() {
	// establish style and event handlers for game board - enterable cells
	//$('table#sudoku').find("[contenteditable='true']")
	//	.addClass('enterable')
	//	.on("focus", onCellFocus )
	//	.on("keypress", keyPress );
	//
	// establish style for non-enterable cells
	// $('table#sudoku').find("[contenteditable='false']").addClass('constant');
	

	$('#nextButton').on("click", nextGame );
	$('#clearButton').on("click", clearEnterable );
	$('#hintButton').on("click", getHint );
	$('#solveButton').on("click", solveBoard );

	loadGames();
}

// get all of the games - only if they are not already loaded...
function loadGames() {
	if(allBoards == null) 
		callService( "/api/v1/player/get_all", getallReceived );
}

function getallReceived(data, status, jqXHR) {
	allBoards = JSON.parse(data);
	currentBoardidx = 0;

	// Begin by showing the first game
	initializeBoard(allBoards[0]);
}

function nextGame() { 
	currentBoardidx += 1;
	if (currentBoardidx == allBoards.length)
		currentBoardidx = 0;

	initializeBoard(allBoards[currentBoardidx]);
}

function initializeBoard(board) {
	var boardArray;
	var col, row, val;
	var cell;

	$('#topTitle').text( 'Board: #' + board.id + ' ' + board.name)

	// clear all classes
	$('table#sudoku td').removeClass();

	boardArray = board.positions.split("");

	for (row = 0; row <=8; row++) {
		for (col=0; col <= 8; col++) {

			cell = document.getElementById('r' + row + 'c' + col);
			val = boardArray[row*9 + col];

			if(val == 0 ) {
				// enterable field
				cell.setAttribute("contentEditable", true);
				cell.innerHTML = "";
				cell.classList.add('enterable');
				$(cell).on("focus", onCellFocus )
					.on("keypress", keyPress );
			}
			else {
				// non-enterable field
				cell.setAttribute("contentEditable", false);
				cell.innerHTML = val;
				cell.classList.add('constant');
			}
		}
	}
}


function clearEnterable() { 
	$('table#sudoku').find("[contenteditable='true']").text("");
}

function getHint() {
	var workBoard = "";

	workBoard = boardToString();

	callService( "/api/v1/player/hint?board=" + workBoard,
					hintReceived
				);
}

function hintReceived(data, status, jqXHR) {
  	var result = JSON.parse(data);

	hintToBoard(result.row, result.col, result.val);
}

function hintToBoard( row, col, val) {
	var cell;

	cell = document.getElementById('r' + row + 'c' + col);

	// val == 0 means hint failed.
	if( val != 0 )
		cell.innerHTML = val;
}

function solveBoard() {
	var workBoard = "";

	workBoard = boardToString();

	callService( "/api/v1/player/solve?board=" + workBoard,
					solveReceived
				);
}

function boardToString() {
	var col, row;
	var workBoard = "";
	var cell;


	for (row = 0; row <=8; row++) {
		for (col=0; col <= 8; col++) {

			cell = document.getElementById('r' + row + 'c' + col);
	
			workBoard += (parseInt(cell.innerHTML) ? cell.innerHTML : "0");
		}
	}

	return workBoard;
}

function solveReceived(data, status, jqXHR) {
  	var result = JSON.parse(data);

	stringToBoard(result.board);
}

function stringToBoard(boardString) {
	var boardArray;
	var col, row, val;
	var cell;

	boardArray = boardString.split("");

	for (row = 0; row <=8; row++) {
		for (col=0; col <= 8; col++) {

			cell = document.getElementById('r' + row + 'c' + col);
			val = boardArray[row*9 + col];

			cell.innerHTML = (val == 0 ? "" : val);
		}
	}
}

function callService( urlToUse, successCallBack ) {
    $.ajax({
      // the URL for the request
      url: urlToUse,

      // whether this is a POST or GET request
      type: "GET",
     
      // the type of data we expect back
      dataType : "text"
    })
    .done(successCallBack)
    .fail(errorFn)
    .always(function (data, textStatus, jqXHR ) {
        console.log("The request is complete!");
    });
}

function errorFn(xhr, status, strErr) {
    console.log("There was an error!");
}

function onCellFocus() {
	// If focus was on an enterable cell 
	// - different than 'this' cell 
	// - reset its formatting

	if (current_cell !== null) {

		// Leaving 'current_cell'

		current_cell.classList.remove('selected');
		current_cell.classList.add('enterable');
	}

	current_cell = this;

	current_cell.classList.remove('enterable');
	current_cell.classList.add('selected');
}

// Capture keyboard key presses. If the key pressed is a digit
// then add it to the current cell. If it is a space then empty
// the current cell.
function keyPress(evt) {
	var keyChar = String.fromCharCode(evt.charCode);
	
	if ( !(keyChar >= 1 && keyChar <= 9) ) {
		current_cell.innerHTML = '';
		return false;
	}
	
	current_cell.innerHTML = keyChar;

	if( IsValidMove(current_cell, keyChar) ) {
		current_cell.classList.remove('cellError');
	}
	else {
		current_cell.classList.add('cellError');
	}
	
	return false;
}

function IsValidMove( cell, keyChar) {
	var row;
	var col;

	// Get current cell row/column values
	row = cell.id.substring(1,2);
	col = cell.id.substring(3,4);

	// MAKE sure new value is not in current row.
	for (var i=0; i<9; i++) { 

		var cell_id="r"+row+"c"+i;

		if(cell_id != cell.id && document.getElementById(cell_id).innerHTML == keyChar)
			return false;
	}

	// MAKE sure new value is not in current column.
	for (var i=0; i<9; i++) { 

		var cell_id="r"+i+"c"+col;

		if(cell_id != cell.id && document.getElementById(cell_id).innerHTML == keyChar)
			return false;
	}

	// MAKE sure new value is not in current square.
	// A bit trickier than rows and columns. 

	// Identify top left most cell of the square the cell is in.
	// squares start at 0, 3, and 6. row and column identifaction is the same.
	//

	top_row = 3 * Math.floor(row/3);
	top_col = 3 * Math.floor(col/3);

	for (var i=top_row; i<top_row+3; i++)
	{ 
		for (var j=top_col; j<top_col+3; j++)
		{ 
			var cell_id="r"+i+"c"+j;

			if(cell_id != cell.id && document.getElementById(cell_id).innerHTML == keyChar) 
				return false;
		}
	}

	return true
}
