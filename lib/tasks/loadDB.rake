require 'rake'

require 'csv'


task :loadDB => :environment do

	desc "Import Sudoku data."
    Board.delete_all
    rowcnt = 0
    file=File.open('lib/tasks/games.csv', "r:ISO-8859-1")
    csv = CSV.new(file, :headers => true, :return_headers => true, :header_converters => [:symbol])
    csv.each do |row|
      rh = row.to_hash
      # rh[:name] = row[:name]
      # rh[:positions] = row[:positions]
      b = Board.new(rh)
      b.save! unless rowcnt == 0
      rowcnt = rowcnt.next
    end
    puts "imported #{rowcnt} rows"
end

