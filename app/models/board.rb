class Board < ActiveRecord::Base

	scope :sorted, -> { order("boards.name ASC") }

	def to_array
		a = []

		buffer = positions
		buffer.gsub!(/\r/,"")				# eliminate EOL
		buffer.gsub!(/\s+/, "")				# Eliminate white space
		buffer.gsub!(",", "")				# Eliminate comma

		a = buffer.scan(/./).map { |s| s.to_i }				# Split into single char strings

		a
	end
end
