
# http://localhost:3000/api/v1/player/hint
#

require 'sudoku_game'

module Player
	class Play < Grape::API
		version 'v1', using: :path
		format :json
		
		resource :player do

			get :get_all do
				boards = Board.sorted
			end

			get :solve do

				g = SudokuGame.new(params[:board])
				g.solve

				response = {}
				response[:board] = g.boardStr

				response
			end

			get :hint do

				g = SudokuGame.new(params[:board])
				r, c, v = g.hint

				response = {}
				response[:row] = r
				response[:col] = c
				response[:val] = v

				response
			end
		end
	end
end

# {"board":"590630704100007060764500398000310600000205000005046000658003429070800006301062057","row":3,"col":4,"value":5}