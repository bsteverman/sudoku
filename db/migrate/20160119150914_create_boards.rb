class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|

    	t.string :name
    	t.string :positions
    	t.string :flags
    	t.integer :current_version

    	t.timestamps null: false
    end
  end
end
